(setq ert-batch-backtrace-right-margin 500)

(custom-set-variables '(lsp-session-file "/tmp/lsp-test-session"))

(load-file "test/test-helper.el")

(ert-run-tests-batch-and-exit
 '(not (or
        (tag org)
        (tag no-win)
        ;; Succeeds during building, fails during autopkgtest when trying to
        ;; overwrite byte-compiled file in system directory but autopkgtest
        ;; doesn't have write permission.
        "lsp-byte-compilation-test"
        ))
 )
